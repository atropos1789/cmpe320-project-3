#! /usr/bin/env julia 
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 3
Functions to bin data, code taken from CMPE320 Project 2
=#


function findmax(data::Vector{Float64})
    # find the maximum value within the dataset
    maxval = 0.0
    for i in 1:length(data)
        (data[i] >= maxval) && (maxval = data[i])
    end
    maxval = ceil(maxval)
    return maxval
end


function findmin(data::Vector{Float64})
    # find the minimum value within the dataset
    minval = 0.0
    for i in 1:length(data)
        (data[i] <= minval) && (minval = data[i])
    end
    minval = floor(minval)
    return minval
end


function create_binvals(binsize, minval, numbins)::Vector{Float64}
    # determine the center of each bin for the data
    return Float64[ k * binsize + minval for k = 0:numbins ]
end

function create_disc_binvals(binsize, minval, maxval)::Vector{Float64}
    rounded_maxval = convert(Int64, round(maxval))
    rounded_minval = convert(Int64, round(minval))
    val_range = abs(rounded_minval) + abs(rounded_maxval)
    return Float64[ k * binsize + minval for k = 0:val_range ]
end


function bin_data(data::Vector{Float64}, bin_size::Rational{Int64}, bin_vals::Vector{Float64}, minval::Float64)::Vector{Float64}
    # transform unbinned data into binned data
    binned_data = zero(bin_vals)
    for i in 1:length(data)
        bin_center = round(data[i]*denominator(binsize))/denominator(binsize)
        bin_num = (bin_center - minval)*denominator(binsize) + 1
        bin_num = convert(Int64, bin_num)
        binned_data[bin_num] += 1
    end
    normed_binned_data = binned_data * denominator(binsize) / length(data)
    return normed_binned_data    
end

function bin_disc_data(data::Vector, minval, maxval)
    rounded_maxval = convert(Int64, round(maxval))
    rounded_minval = convert(Int64, round(minval))
    binned_data = zeros(abs(rounded_minval) + abs(rounded_maxval) + 1)
    for i in 1:length(data)
        binned_data[convert(Int64, round(data[i])) - rounded_minval + 1] += 1
    end
    normed_binned_data = binned_data / length(data)
    return normed_binned_data    
end
