#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Project 3
Code entry point - run this file to run the entire project's code!
=#

# include the file corresponding to each problem 
include("problem-2-1.jl")
include("problem-2-2.jl")
include("problem-2-3.jl")
include("problem-2-4.jl")
