#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu

=#

using Random
using Distributions
using PyPlot
include("data-binning.jl")

n::Int64 = 100000
binsize::Rational{Int64} = 1//4
theta::Float64 = 10.0/7.0
# Distributions.jl uses an alternative parameterization of the 
# exponential distribution where θ = 1/λ
Prob3_distribution = Distributions.Exponential(theta)
theoretical_mean::Float64 = theta
theoretical_variance::Float64 = theta^2

N1::Int64 = 5
N1_theoretical_mean::Float64 = N1 * theoretical_mean
N1_theoretical_variance::Float64 = N1 * theoretical_variance

N2::Int64 = 40
N2_theoretical_mean::Float64 = N2*theoretical_mean
N2_theoretical_variance::Float64 = N2*theoretical_variance

N3::Int64 = 200
N3_theoretical_mean::Float64 = N3*theoretical_mean
N3_theoretical_variance::Float64 = N3*theoretical_variance

N_values::Vector{Int64} = [N1, N2, N3]
N_data = zeros(n, length(N_values))
N_means = zeros(length(N_values))
N_variances = zeros(length(N_values))
P_pdf(x, mean, variance) = 1/sqrt(2.0*pi*variance) * exp( -0.5*(x - mean)^2/variance)

for i in 1:length(N_values)
    N = N_values[i]

    # a similar effect could be gotten by computing n different arrays
    # of size N of random values and summing each individually, 
    # however this would be unoptimized compared to this implementation
    current_data = zeros(n, N)
    current_data = Random.rand!(Prob3_distribution, current_data)
    N_data[:,i] = sum(current_data, dims=2)

    # computing the mean
    current_mean = sum(N_data[:,i])/length(N_data[:,i])
    N_means[i] = current_mean

    # computing the variance
    current_variance = var(N_data[:,i])
    N_variances[i] = current_variance
end

N1_data = N_data[:,1]
N1_mean = N_means[1]
N1_variance = N_variances[1]

N2_data = N_data[:,2]
N2_mean = N_means[2]
N2_variance = N_variances[2]

N3_data = N_data[:,3]
N3_mean = N_means[3]
N3_variance = N_variances[3]

# graphs

maxval_N1 = findmax(N1_data)
minval_N1 = findmin(N1_data)
numbins_N1 = (maxval_N1 - minval_N1) * denominator(binsize)
binvals_N1 = create_binvals(binsize, minval_N1, numbins_N1)
binned_N1 = bin_data(N1_data, binsize, binvals_N1, minval_N1)

figure(7)
xlabel(raw"$p$")
ylabel(raw"Probability (Density), $P=\sum_{N} \text{Exp}(0.7)$")
title(raw"Experimental data versus $f_P(p)$ for $N=5$")
# histogram needs to be aligned left because of unknown reasons,
# but this is necessary to have properly centered bins
hist(binvals_N1, bins=binvals_N1, weights=binned_N1, align="left", label="data")
x = range(minval_N1, step=0.01, stop=maxval_N1)
plot(x, P_pdf.(x, N1_theoretical_mean, N1_theoretical_variance), label=raw"$f_P(p)$")
figlegend()
savefig("../images/problem-2-3-fig-1-hist.png", dpi=288)

maxval_N2 = findmax(N2_data)
minval_N2 = findmin(N2_data)
numbins_N2 = (maxval_N2 - minval_N2) * denominator(binsize)
binvals_N2 = create_binvals(binsize, minval_N2, numbins_N2)
binned_N2 = bin_data(N2_data, binsize, binvals_N2, minval_N2)

figure(8)
xlabel(raw"$p$")
ylabel(raw"Probability (Density), $P=\sum_{N} \text{Exp}(0.7)$")
title(raw"Experimental data versus $f_P(p)$ for $N=50$")
hist(binvals_N2, bins=binvals_N2, weights=binned_N2, align="left", label="data")
y = range(minval_N2, step=0.01, stop=maxval_N2)
plot(y, P_pdf.(y, N2_theoretical_mean, N2_theoretical_variance), label=raw"$f_P(p)$")
figlegend()
savefig("../images/problem-2-3-fig-2-hist.png", dpi=288)


maxval_N3 = findmax(N3_data)
minval_N3 = findmin(N3_data)
numbins_N3 = (maxval_N3 - minval_N3) * denominator(binsize)
binvals_N3 = create_binvals(binsize, minval_N3, numbins_N3)
binned_N3 = bin_data(N3_data, binsize, binvals_N3, minval_N3)


figure(9)
xlabel(raw"$p$")
ylabel(raw"Probability (Density), $P=\sum_{N} \text{Exp}(0.7)$")
title(raw"Experimental data versus $f_P(p)$ for $N=200$")
hist(binvals_N3, bins=binvals_N3, weights=binned_N3, align="left", label="data")
z = range(minval_N3, step=0.01, stop=maxval_N3)
plot(z, P_pdf.(z, N3_theoretical_mean, N3_theoretical_variance), label=raw"$f_P(p)$")
figlegend()
savefig("../images/problem-2-3-fig-3-hist.png", dpi=288)


# print out data

println("Data for N = 5")
println("mean = ", N1_mean)
println("variance = ", N1_variance)
println("theoretical mean = ", N1_theoretical_mean)
println("theoretical variance = ", N1_theoretical_variance)

println("Data for N = 40")
println("mean = ", N2_mean)
println("variance = ", N2_variance)
println("theoretical mean = ", N2_theoretical_mean)
println("theoretical variance = ", N2_theoretical_variance)

println("Data for N = 200")
println("mean = ", N3_mean)
println("variance = ", N3_variance)
println("theoretical mean = ", N3_theoretical_mean)
println("theoretical variance = ", N3_theoretical_variance)



