#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
The solution to problem 4 of project 3
=#


using Random
using Distributions
using PyPlot
include("data-binning.jl")


## Problem Parameters

n::Int64 = 100000
binsize::Rational{Int64} = 1//1
Prob1_distribution = Distributions.Bernoulli(0.6)

N1::Int64 = 4
N1_theoretical_mean::Float64 = 2.4
N1_theoretical_variance::Float64 = 0.96

N2::Int64 = 20
N2_theoretical_mean::Float64 = 12.0
N2_theoretical_variance::Float64 = 4.8

N3::Int64 = 100
N3_theoretical_mean::Float64 = 60.0
N3_theoretical_variance::Float64 = 24.0


## Problem Data Structures

N_values::Vector{Int64} = [N1, N2, N3]
N_data = zeros(n, length(N_values))
N_means = zeros(length(N_values))
N_variances = zeros(length(N_values))
P_pdf(x, mean, variance) = 1/sqrt(2.0*pi*variance) * exp( -0.5*(x - mean)^2/variance)
P_pmf(k, N, p) = binomial(N, k) * p^k * (1-p)^(N-k)
p_val::Float64 = 0.6

## Create Data

for i in 1:length(N_values)
    N = N_values[i]

    # a similar effect could be gotten by computing n different arrays
    # of size N of random values and summing each individually, 
    # however this would be unoptimized compared to this implementation
    current_data = zeros(n, N)
    current_data = Random.rand!(Prob1_distribution, current_data)
    N_data[:,i] = sum(current_data, dims=2)

    # computing the mean
    current_mean = sum(N_data[:,i])/length(N_data[:,i])
    N_means[i] = current_mean

    # computing the variance
    current_variance = var(N_data[:,i])
    N_variances[i] = current_variance
end

N1_data = N_data[:,1]
N1_mean = N_means[1]
N1_variance = N_variances[1]

N2_data = N_data[:,2]
N2_mean = N_means[2]
N2_variance = N_variances[2]

N3_data = N_data[:,3]
N3_mean = N_means[3]
N3_variance = N_variances[3]


## Bin, Plot Data and the PDF

# N=4
maxval_N1 = findmax(N1_data)
minval_N1 = findmin(N1_data)
numbins_N1 = (maxval_N1 - minval_N1)
binvals_N1 = create_disc_binvals(binsize, minval_N1, maxval_N1)
binned_N1 = bin_disc_data(N1_data, minval_N1, maxval_N1)

figure(10)
xlabel(raw"$k$")
ylabel(raw"Probability (Density), $K=\sum_{N} Bernoulli(0.6)$")
title(raw"Experimental data versus $f_K(k)$ for $N=4$")
# histogram needs to be aligned left because of unknown reasons,
# but this is necessary to have properly centered bins
hist(binvals_N1, bins=binvals_N1, weights=binned_N1, align="left", label="data")
x = range(convert(Int64, round(minval_N1)), length=convert(Int64, round(maxval_N1)))
stem(x, P_pmf.(x, N1, p_val), label=raw"$f_K(k)$", linefmt="C1-")
figlegend()
savefig("../images/problem-2-4-fig-1.png", dpi=288)

figure(11)
xlabel(raw"$p$")
ylabel(raw"Probability (Density), $K=\sum_{N} Bernoulli(0.6)$")
title(raw"Experimental data versus $f_P(p)$ for $N=4$")
# histogram needs to be aligned left because of unknown reasons,
# but this is necessary to have properly centered bins
hist(binvals_N1, bins=binvals_N1, weights=binned_N1, align="left", label="data")
x = range(minval_N1, step=0.01, stop=maxval_N1)
plot(x, P_pdf.(x, N1_theoretical_mean, N1_theoretical_variance), label=raw"$f_P(p)$")
figlegend()
savefig("../images/problem-2-4-fig-2.png", dpi=288)


# N=20
maxval_N2 = findmax(N2_data)
minval_N2 = findmin(N2_data)
numbins_N2 = (maxval_N2 - minval_N2) * denominator(binsize)
binvals_N2 = create_disc_binvals(binsize, minval_N2, maxval_N2)
binned_N2 = bin_disc_data(N2_data, minval_N2, maxval_N2)

figure(12)
xlabel(raw"$k$")
ylabel(raw"Probability (Density), $K=\sum_{N} Bernoulli(0.6)$")
title(raw"Experimental data versus $f_K(k)$ for $N=20$")
hist(binvals_N2, bins=binvals_N2, weights=binned_N2, align="left", label="data")
y = range(convert(Int64, round(minval_N2)), length=convert(Int64, round(maxval_N2)))
stem(y, P_pmf.(y, N2, p_val), label=raw"$f_K(k)$", linefmt="C1-")
figlegend()
savefig("../images/problem-2-4-fig-3.png", dpi=288)


figure(13)
xlabel(raw"$p$")
ylabel(raw"Probability (Density), $K=\sum_{N} Bernoulli(0.6)$")
title(raw"Experimental data versus $f_P(p)$ for $N=20$")
hist(binvals_N2, bins=binvals_N2, weights=binned_N2, align="left", label="data")
y = range(minval_N2, step=0.01, stop=maxval_N2)
plot(y, P_pdf.(y, N2_theoretical_mean, N2_theoretical_variance), label=raw"$f_P(p)$")
figlegend()
savefig("../images/problem-2-4-fig-4.png", dpi=288)


# N=100
maxval_N3 = findmax(N3_data)
minval_N3 = findmin(N3_data)
numbins_N3 = (maxval_N3 - minval_N3) * denominator(binsize)
binvals_N3 = create_disc_binvals(binsize, minval_N3, maxval_N3)
binned_N3 = bin_disc_data(N3_data, minval_N3, maxval_N3)

figure(14)
xlabel(raw"$k$")
ylabel(raw"Probability (Density), $K=\sum_{N} Bernoulli(0.6)$")
title(raw"Experimental data versus $f_K(k)$ for $N=100$")
hist(binvals_N3, bins=binvals_N3, weights=binned_N3, align="left", label="data")
z = range(convert(Int128, round(minval_N3)), length=convert(Int64, round(maxval_N3)))
stem(z, P_pmf.(z, N3, p_val), label=raw"$f_K(k)$", linefmt="C1-")
figlegend()
savefig("../images/problem-2-4-fig-5.png", dpi=288)

figure(15)
xlabel(raw"$p$")
ylabel(raw"Probability (Density), $K=\sum_{N} Bernoulli(0.6)$")
title(raw"Experimental data versus $f_P(p)$ for $N=100$")
hist(binvals_N3, bins=binvals_N3, weights=binned_N3, align="left", label="data")
z = range(minval_N3, step=0.01, stop=maxval_N3)
plot(z, P_pdf.(z, N3_theoretical_mean, N3_theoretical_variance), label=raw"$f_P(p)$")
figlegend()
savefig("../images/problem-2-4-fig-6.png", dpi=288)


# print out data

println("Data for N = 4")
println("mean = ", N1_mean)
println("variance = ", N1_variance)
println("theoretical mean = ", N1_theoretical_mean)
println("theoretical variance = ", N1_theoretical_variance)

println("Data for N = 20")
println("mean = ", N2_mean)
println("variance = ", N2_variance)
println("theoretical mean = ", N2_theoretical_mean)
println("theoretical variance = ", N2_theoretical_variance)

println("Data for N = 100")
println("mean = ", N3_mean)
println("variance = ", N3_variance)
println("theoretical mean = ", N3_theoretical_mean)
println("theoretical variance = ", N3_theoretical_variance)
